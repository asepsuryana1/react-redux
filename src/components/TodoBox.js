import React, { Component } from 'react';
import TodoList from '../containers/TodoList';
import TodoForm from '../containers/TodoForm';

export default class TodoBox extends Component {

    render() {
        return (
            <div>
                <h1>Daftar TODO</h1>
                <TodoList />
                <TodoForm />
            </div>
        )
    }
}