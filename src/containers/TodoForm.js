import React, { Component } from 'react';
import { postTodo } from '../actions';
import { connect } from 'react-redux';


class TodoForm extends Component {
    constructor(props) {
        super(props);
        this.state = { value: '' };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({ value: event.target.value });
    }

    handleSubmit(event) {
        this.props.addTodo(this.state.value)
        this.setState({ value: '' })
        event.preventDefault();
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <label>
                    Task:
            <input type="text" value={this.state.value} onChange={this.handleChange} />
                </label>
                <input type="submit" value="Submit" />
            </form>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({
    addTodo: (task) => dispatch(postTodo(task))
  })
  
  export default connect(
    null,
    mapDispatchToProps
  )(TodoForm)