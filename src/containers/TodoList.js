import React, { Component } from 'react';
import TodoItem from './Todo';
import {loadTodo} from '../actions';
import { connect } from 'react-redux';

class TodoList extends Component {

    componentDidMount() {
        this.props.loadTodo()
    }

    render() {
        console.log(this.props.todos)
        const listItems = this.props.todos.map((item, index) =>
            <TodoItem key={index} todo={{...item}} />
        );

        return (
            <ol>
                {listItems}
            </ol>
        )
    }
}

const mapStateToProps = (state) => ({
    todos: state.todos
})

const mapDispatchToProps = (dispatch) => ({
    loadTodo: () => dispatch(loadTodo())
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(TodoList)