import axios from 'axios'

const API_URL = 'http://localhost:3000/api/'

const request = axios.create({
  baseURL: API_URL,
  timeout: 1000
});

// start load todo data
const loadTodoSuccess = (todos) => ({
  type: 'LOAD_TODO_SUCCESS',
  todos
})

const loadTodoFailure = () => ({
  type: 'LOAD_TODO_FAILURE'
})

export const loadTodo = () => {
  return dispatch => {
    return request.get('todos')
    .then(function (response) {
      dispatch(loadTodoSuccess(response.data))
    })
    .catch(function (error) {
      console.error(error);
      dispatch(loadTodoFailure())
    });
  }
}

// end load todo data

// start post todo data

const postTodoSuccess = (todo) => ({
  type: 'POST_TODO_SUCCESS',
  todo
})

const postTodoFailure = (id) => ({
  type: 'POST_TODO_FAILURE', id
})

const postTodoRedux = (id, task) => ({
  type: 'POST_TODO', id, task
})


export const postTodo = (task) => {
  let id = Date.now();
  return dispatch => {
    dispatch(postTodoRedux(id, task))
    return request.post('todos', {id, task})
    .then(function (response) {
      dispatch(postTodoSuccess(response.data))
    })
    .catch(function (error) {
      console.error(error);
      dispatch(postTodoFailure(id))
    });
  }
}

// start delete todo data

const deleteTodoRedux = (id) => ({
  type: 'DELETE_TODO', id
})

const deleteTodoSuccess = (todos) => ({
  type: 'DELETE_TODO_SUCCESS',
  todos
})

const deleteTodoFailure = () => ({
  type: 'DELETE_TODO_FAILURE'
})


export const deleteTodo = (id) => {
  return dispatch => {
    dispatch(deleteTodoRedux(id))
    return request.delete(`todos/${id}`)
    .then(function (response) {
      dispatch(deleteTodoSuccess(response.data))
    })
    .catch(function (error) {
      console.error(error);
      dispatch(deleteTodoFailure())
    });
  }
}

// end delete todo data

export const resendTodo = (id, task) => {
  return dispatch => {
    return request.post('todos', {id, task})
    .then(function (response) {
      dispatch(postTodoSuccess(response.data))
    })
    .catch(function (error) {
      console.error(error);
      dispatch(postTodoFailure(id))
    });
  }
}