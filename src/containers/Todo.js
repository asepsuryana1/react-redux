import { connect } from 'react-redux'
import { deleteTodo, resendTodo } from '../actions'
import TodoItem from '../components/TodoItem'

const mapDispatchToProps = (dispatch, ownProps) => ({
  remove: () => dispatch(deleteTodo(ownProps.todo.id)),
  resend: () => dispatch(resendTodo(ownProps.todo.id, ownProps.todo.task))
})

export default connect(
  null,
  mapDispatchToProps
)(TodoItem)