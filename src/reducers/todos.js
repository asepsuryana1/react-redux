const todos = (state = [], action) => {
  switch (action.type) {
    case 'LOAD_TODO_SUCCESS':
      return action.todos.map((item) => {
        item.sent = true;
        return item
      })

    case 'POST_TODO':
      return [
        ...state,
        {
          id: action.id,
          task: action.task,
          sent: true
        }
      ]

    case 'POST_TODO_SUCCESS':
      return state.map((item) => {
        if (item.id === action.todo.id) {
          item.sent = true;
        }
        return item
      })

    case 'POST_TODO_FAILURE':
      console.log(typeof action.id, action.id)
      return state.map((item) => {
        if (item.id === action.id) {
          item.sent = false;
        }
        return item
      })

    case 'DELETE_TODO':
      return state.filter((item) => item.id !== action.id)

    case 'DELETE_TODO_SUCCESS':
      return state

    case 'LOAD_TODO_FAILURE':
    case 'DELETE_TODO_FAILURE':
    default:
      return state
  }
}

export default todos